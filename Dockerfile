FROM node:10-alpine

WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN cp .env.dev .env

CMD [ "npm", "start" ]
