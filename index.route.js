const express = require("express");
const userRoutes = require("./server/User/user.route");
const authRoutes = require("./server/Authentification/authentification.route");
const rdvRoutes = require("./server/Rendez_vous/rendez_vous.route");
const consultationRoutes = require("./server/Consultations/consultation.route");
const specialiteRoutes = require("./server/Specialite/specialite.route");
const villeRoutes = require("./server/Ville/ville.route");
const disponibilityRoutes = require("./server/Disponibility/disponibility.route");
const medicamentRoutes = require("./server/Medicaments/medicament.route");
const imagesRoutes = require("./server/Image/image.route");

const router = express.Router();
// TODO: use glob to match *.route files

/** GET /health-check - Check service health */
router.get("/health-check", (req, res) => res.send("OK"));

// mount auth routes at /auth
router.use("/auth", authRoutes);

// mount user routes at /users
router.use("/users", userRoutes);

//specialite routes at /specialite
router.use("/specialites", specialiteRoutes);

//ville routes at /ville
router.use("/villes", villeRoutes);

// mount rdv routes at /rendez_vous
router.use("/rendez-vous", rdvRoutes);

// mount consultation routes at /consultations
router.use("/consultations", consultationRoutes);

router.use("/disponibility", disponibilityRoutes);

router.use("/medicaments", medicamentRoutes);

router.use("/images", imagesRoutes);

module.exports = router;
