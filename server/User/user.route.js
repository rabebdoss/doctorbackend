const express = require("express");
const userCtrl = require("./user.controller");
const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** GET /api/users - Get list of users */
  .get(userCtrl.list)

  /** POST /api/users - Create new user */
  .post(userCtrl.create);
router.route("/verifier/:userId").put(userCtrl.verifier);

router.route("/verifier").get(userCtrl.searchMedecin);
router.route("/uploadCin").put(userCtrl.uploadImageCin);
router
  .route("/:userId")
  /** PUT /api/users/5cb0a81bee45a4139cd88fdd - Update user */
  .put(userCtrl.update)
  .get(userCtrl.get)
  /** DELETE /api/users/5cb0a81bee45a4139cd88fdd - Delete user */
  .delete(userCtrl.remove);

router.route("/ville/:ville/specialty/:specialty").get(userCtrl.searchUser);

module.exports = router;
