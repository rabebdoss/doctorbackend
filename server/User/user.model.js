const mongoose = require("mongoose");

/**
 * User Schema
 */
const UserSchema = new mongoose.Schema({
  userImage: {
    type: String,
    default: null
    //require: true
  },
  first_name: {
    type: String
    //require: true
    //match: [a-zA-Z]+$
  },
  last_name: {
    type: String
    //require: true
    //match: [a-zA-Z]+$
  },
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true
    /*validate: {
      validator: validator.isEmail,
      message: "{VALUE} is not a valid email"
    }*/
  },

  password: {
    type: String,
    required: true
    //Minimum eight characters, at least one letter and one number.
    //match:["^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"]
  },
  mobile_number: {
    type: Number,
    default: null
    //required: true
    //match: [/^[1-9][0-9]{7}$/, 'The value of path {PATH} ({VALUE}) is not a valid mobile number.']
  },
  date_naissance: { type: Date, default: null },
  adresse: { type: String },

  civility: { type: String, enum: ["Femme", "Homme"] },

  user_type: {
    type: String,
    enum: ["Admin", "Patient", "Medecin", "Secretaire"]
  },

  doctor: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  ville: { type: mongoose.Schema.Types.ObjectId, ref: "Ville" },
  specialty: { type: mongoose.Schema.Types.ObjectId, ref: "Specialite" },
  disponibility: { type: mongoose.Schema.Types.ObjectId, ref: "Disponibility" },
  formation: {
    type: String
    //required: true,
  },

  professional_experience: {
    type: String
    //required: true,
  },
  cinImage: {
    type: String,
    default: null
    //require: true
  },
  verified: {
    type: Boolean,
    default: false
  },
  token: {
    type: String,
    default: null
  }
});
/** * Methods */
UserSchema.method({});

/** * Statics */
UserSchema.statics = {
  /**   * List users in descending order of 'createdAt' timestamp.     */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

module.exports = mongoose.model("User", UserSchema);
