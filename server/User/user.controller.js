const User = require("./user.model");
var sha512 = require("js-sha512");
const Disponibility = require("../Disponibility/disponibility.model");
const disponibilityCtrl = require("../Disponibility/disponibility.controller");

/** * Load user and append to req */
function load(req, res, next, id) {
  User.get(id)
    .then(user => {
      req.user = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}
/** * Get user */
function get(req, res) {
  User.findOne({ _id: req.params.userId })
    .select(["-password", "-token"])
    .populate("specialty")
    .populate("ville")
    .then(user => {
      if (user) {
        res.status(200).json(user);
      } else {
        res.status(404).send("User not found");
      }
    })
    .catch(e => console.log(e));
}

function searchUser(req, res) {
  let options = {
    user_type: "Medecin",
    verified: true
  };
  if (req.params.ville !== "null") {
    options.ville = req.params.ville;
  }
  if (req.params.specialty !== "null") {
    options.specialty = req.params.specialty;
  }
  User.find(options)
    .select(["-password", "-token"])
    .populate("specialty")
    .populate("ville")
    .populate("disponibility")
    .then(users => {
      if (users) {
        res.status(200).json(users);
      } else {
        res.status(404).send("User not found");
      }
    })
    .catch(e => console.log(e));
}

function searchMedecin(req, res) {
  User.find({ user_type: "Medecin", verified: false })
    .select(["-password", "-token"])
    .then(users => res.json(users))
    .catch(e => next(e));
}

/** * Create new user */
function create(req, res, next) {
  let user = new User();

  user.first_name = req.body.first_name;
  user.last_name = req.body.last_name;
  user.email = req.body.email;
  user.password = sha512(req.body.password);
  user.mobile_number = req.body.mobile_number;
  user.date_naissance = req.body.date_naissance;
  user.adresse = req.body.adresse;
  user.civility = req.body.civility;
  user.user_type = req.body.user_type;
  user.verified = req.body.user_type === "Patient";
  user.ville = req.body.ville;
  user.specialty = req.body.specialty;
  user.formation = req.body.formation;
  user.professional_experience = req.body.professional_experience;
  user.cinImage = req.body.cinImage;
  user.doctor = req.body.doctor;
  if (req.body.user_type === "Medecin") {
    const disponibility = createDisponibility(user._id);
    user.disponibility = disponibility._id;
    // disponibilityCtrl.options();
    user.save(e => {
      if (e) {
        res.status(406).send(e);
      } else {
        disponibility.save(e => {
          if (e) {
            res.status(406).send(e);
          }
          res.status(201).json(user);
        });
      }
    });
  } else {
    user.save(e => {
      if (e) {
        res.status(406).send(e);
      } else {
        res.status(201).json(user);
      }
    });
  }
}
function uploadImageCin(req, res, next) {
  uploadCin(req, res, function(err) {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
    }
    if (req.file && req.file.filename) {
      return res.send(`imagesCin/${req.file.filename}`);
    }
    return res.status(406).json({ message: "no file found" });
  });
}
/************** upload image *********** */
var multer = require("multer");

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "uploads/");
  },
  filename: function(req, file, cb) {
    const mimetypes = file.mimetype.split("/");
    cb(null, file.fieldname + "-" + Date.now() + "." + mimetypes[1]);
  }
});
var storageCin = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "uploadsCin/");
  },
  filename: function(req, file, cb) {
    const mimetypes = file.mimetype.split("/");
    cb(null, file.fieldname + "-" + Date.now() + "." + mimetypes[1]);
  }
});
var upload = multer({ storage: storage }).single("profileImage");
var uploadCin = multer({ storage: storageCin }).single("cinImage");

/** * Update existing user  */
function update(req, res, next) {
  upload(req, res, function(err) {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
    }
    let user = {};

    if (req.file && req.file.filename) {
      user.userImage = `images/${req.file.filename}`;
    }
    if (req.body.first_name) {
      user.first_name = req.body.first_name;
    }
    if (req.body.last_name) {
      user.last_name = req.body.last_name;
    }
    if (req.body.mobile_number) {
      user.mobile_number = req.body.mobile_number;
    }
    if (req.body.date_naissance) {
      user.date_naissance = req.body.date_naissance;
    }
    if (req.body.adresse) {
      user.adresse = req.body.adresse;
    }
    if (req.body.civility) {
      user.civility = req.body.civility;
    }
    if (req.body.ville) {
      user.ville = req.body.ville;
    }
    if (req.body.specialty) {
      user.specialty = req.body.specialty;
    }
    if (req.body.formation) {
      user.formation = req.body.formation;
    }
    if (req.body.professional_experience) {
      user.professional_experience = req.body.professional_experience;
    }
    User.findByIdAndUpdate(req.params.userId, user, { new: true })
      .then(user => res.json(user))
      .catch(e => res.status(400).send(e));
  });
}

/** * Get user list */
function list(req, res, next) {
  User.find({})
    .populate("specialty")
    .populate("ville")
    .then(users => res.json(users))
    .catch(e => next(e));
}

/** * Delete user */
function remove(req, res) {
  User.findOneAndDelete(req.params.userId)
    .then(user => res.json(user))
    .catch(e => res.status(400).send(e));
}

/*** Verifier medecin */

function verifier(req, res, next) {
  User.findByIdAndUpdate(req.params.userId, { verified: true })
    .then(user => res.json(user))
    .catch(e => res.status(400).send(e));
}

const createDisponibility = idUser => {
  const disponibility = new Disponibility();
  disponibility.userId = idUser;
  const tab = [];
  const days = [
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
    "Dimanche"
  ];
  for (let index = 0; index < days.length; index++) {
    tab.push({
      name: days[index],
      startTime: "0:0",
      endTime: "0:0",
      active: false
    });
  }
  disponibility.disponibilite = tab;
  disponibility.period = 0;
  return disponibility;
};

module.exports = {
  load,
  get,
  create,
  update,
  list,
  remove,
  searchUser,
  searchMedecin,
  verifier,
  uploadImageCin
};
