const Medicament = require("./medicament.model");

/** * Create new Specialite */
function create(req, res, next) {
  let medicament = new Medicament();
  medicament.name = req.body.name;
  medicament.description = req.body.description;
  medicament.save(e => {
    if (e) {
      res.status(406).send(e);
    } else {
      res.status(201).json(medicament);
    }
  });
}

function getMedicament(req, res, next) {
  Medicament.find(req.params.id)
    .then(medicament => res.json(medicament))
    .catch(e => res.status(400).send(e));
}

/** * Get Medicament list */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Medicament.list({ limit, skip })
    .then(medicaments => res.json(medicaments))
    .catch(e => next(e));
}

module.exports = { create, list, getMedicament };
