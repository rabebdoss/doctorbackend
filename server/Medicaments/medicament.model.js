const mongoose = require("mongoose");

const MedicamentSchema = new mongoose.Schema({
  name: {
    type: String
  },
  description :{
    type:String
  }
});
/** * Methods */
MedicamentSchema.method({});

/** * Statics */
MedicamentSchema.statics = {
  /**   * List Medicament in descending order of 'createdAt' timestamp.     */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

module.exports = mongoose.model("Medicament", MedicamentSchema);
