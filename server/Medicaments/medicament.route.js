const express = require("express");
const medicamentCtrl = require("./medicament.controller");

const router = express.Router();

router
  .route("/")
  /** GET /api/medicament - Get list of medicaments */
  .get(medicamentCtrl.list)
  /** POST /api/medicament - Create new medicament */
  .post(medicamentCtrl.create);

router.route("/:id").get(medicamentCtrl.getMedicament);


module.exports = router;
