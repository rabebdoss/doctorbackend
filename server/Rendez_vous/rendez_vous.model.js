const Promise = require("bluebird");
const mongoose = require("mongoose");
const httpStatus = require("http-status");

/** * User Schema */
const RendezVousSchema = new mongoose.Schema({
  patientId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  medecinId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  disponibilityId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Disponibility"
  },
  date: {
    type: String
  },
  time: {
    type: String
  },
  etat: {
    type: Number
  }
});

/** * Methods */
RendezVousSchema.method({});

/** * Statics */
RendezVousSchema.statics = {
  /**   * List Rdvs in descending order of 'createdAt' timestamp  */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};
module.exports = mongoose.model("RendezVous", RendezVousSchema);
