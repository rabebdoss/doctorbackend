const express = require("express");

const rendezVousCtrl = require("./rendez_vous.controller");

const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** GET /api/rendez-vous - Get list of rendezVous */
  .get(rendezVousCtrl.list)

  /** POST /api/rendez-vous - Create new rendezVous */
  .post(rendezVousCtrl.create);

router
  .route("/:rendezVousId")
  /** PUT /api/rendez-vous/:rendezVousId - Update rdv */
  .put(rendezVousCtrl.update)

  /** DELETE /api/rendez-vous/:rendezVousId - Delete rdv */
  .delete(rendezVousCtrl.remove);

router
  .route("/medecin/:idMedecin/date/:date")
  .get(rendezVousCtrl.getRenderVousByDoctor);

router.route("/patient/:idpatient").get(rendezVousCtrl.getRenderVousByPatient);
router.route("/medecin/:idMedecin").get(rendezVousCtrl.getRenderVousByMedecin);

router
  .route("/patient/:idpatient/notifications")
  .get(rendezVousCtrl.getNotificationByPatient);
router
  .route("/medecin/:idMedecin/notifications")
  .get(rendezVousCtrl.getNotificationByMedecin);



module.exports = router;
