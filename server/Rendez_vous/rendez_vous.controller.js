const RendezVous = require("./rendez_vous.model");
const Consultation = require("../Consultations/consultation.model");
/** * Create new rendezVous */
function create(req, res, next) {
  RendezVous.findOne({
    patientId: req.body.patientId,
    etat: 0,
    medecinId: req.body.medecinId
  })
    .then(rendezVous => {
      if (rendezVous) {
        res.status(406).json({
          message: "vous avez déjà un rendez-vous en attente"
        });
      } else {
        let rendezVous = new RendezVous();
        rendezVous.patientId = req.body.patientId;
        rendezVous.medecinId = req.body.medecinId;
        rendezVous.disponibilityId = req.body.disponibilityId;
        rendezVous.date = req.body.date;
        rendezVous.time = req.body.time;
        rendezVous.etat = req.body.etat;
        rendezVous.save(e => {
          if (e) {
            res.status(406).send(e);
          } else {
            res.status(201).json(rendezVous);
          }
        });
      }
    })
    .catch(e => res.status(400).send(e));
}

/** * Update existing rendezVous  */
function update(req, res, next) {
  RendezVous.findByIdAndUpdate(
    req.params.rendezVousId,
    {
      etat: req.body.etat
    },
    { new: true }
  )
    .then(rendezVous => {
      if (req.body.etat === 1) {
        let consultation = new Consultation();
        consultation.rendezVous = rendezVous._id;
        consultation.medecin = rendezVous.medecinId;
        consultation.patient = rendezVous.patientId;
        consultation.save(e => {
          if (e) {
            res.status(406).send(e);
          } else {
            res.json(rendezVous);
          }
        });
      } else {
        res.json(rendezVous);
      }
    })
    .catch(e => res.status(400).send(e));
}

/**** get rendezVous  Patient */
function getRenderVousByPatient(req, res, next) {
  RendezVous.find({ patientId: req.params.idpatient, etat: 0 })
    //.populate("medecinId", "first_name last_name userImage ville specialty")
    .populate({
      path: "medecinId",
      select: "first_name last_name userImage  specialty",
      populate: { path: "specialty" }
    })

    .then(rendezVous => res.json(rendezVous))
    .catch(e => res.status(400).send(e));
}
/**** get rendezVous  Medecin */
function getRenderVousByMedecin(req, res, next) {
  RendezVous.find({ medecinId: req.params.idMedecin, etat: 0 })
    .populate("patientId", "first_name last_name userImage ")
    .then(rendezVous => res.json(rendezVous))
    .catch(e => res.status(400).send(e));
}

/**** get rendezVous  Patient */
function getNotificationByPatient(req, res, next) {
  RendezVous.find({ patientId: req.params.idpatient, etat: { $ne: 0 } })
    //.populate("medecinId", "first_name last_name userImage ville specialty")
    .populate({
      path: "medecinId",
      select: "first_name last_name userImage  specialty",
      populate: { path: "specialty" }
    })

    .then(rendezVous => res.json(rendezVous))
    .catch(e => res.status(400).send(e));
}
/**** get rendezVous  Medecin */
function getNotificationByMedecin(req, res, next) {
  RendezVous.find({ medecinId: req.params.idMedecin, etat: { $ne: 0 } })
    .populate("patientId", "first_name last_name userImage ")
    .then(rendezVous => res.json(rendezVous))
    .catch(e => res.status(400).send(e));
}

/** * Get rendezVous list */
function getRenderVousByDoctor(req, res, next) {
  RendezVous.find({ medecinId: req.params.idMedecin, date: req.params.date })
    .populate("patientId", "first_name last_name userImage mobile_number")
    .then(rendezVous => res.json(rendezVous))
    .catch(e => res.status(400).send(e));
}

/** * Get rendezVous list */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  RendezVous.list({ limit, skip })
    .then(rendezVous => res.json(rendezVous))
    .catch(e => next(e));
}

/** * Delete rendezVous */
function remove(req, res) {
  RendezVous.findOneAndDelete(req.params.rendezVousId)
    .then(rendezVous => res.json(rendezVous))
    .catch(e => res.status(400).send(e));
}
module.exports = {
  create,
  update,
  list,
  remove,
  getRenderVousByDoctor,
  getRenderVousByPatient,
  getRenderVousByMedecin,
  getNotificationByPatient,
  getNotificationByMedecin
};
