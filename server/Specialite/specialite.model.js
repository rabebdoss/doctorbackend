const Promise = require("bluebird");
const mongoose = require("mongoose");
const httpStatus = require("http-status");


/**
 * Specialite Schema
 */
const SpecialiteSchema = new mongoose.Schema({
  name: {
    type: String
    //require: true
  }
});
/** * Methods */
SpecialiteSchema.method({});

/** * Statics */
SpecialiteSchema.statics = {
  /**   * List Specialite in descending order of 'createdAt' timestamp.     */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

module.exports = mongoose.model("Specialite", SpecialiteSchema);
