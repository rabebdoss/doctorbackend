const Specialite = require("./specialite.model");
var sha512 = require("js-sha512");

/** * Create new Specialite */
function create(req, res, next) {
  let specialite = new Specialite();
  specialite.name = req.body.name;
  specialite.save(e => {
    if (e) {
      res.status(406).send(e);
    } else {
      res.status(201).json(specialite);
    }
  });
}

/** * Update existing Specialite  */
function update(req, res, next) {
  Specialite.findByIdAndUpdate(
    req.params.specialiteId,
    {
      name: req.body.name
    },
    { new: true }
  )
    .then(specialite => res.json(specialite))
    .catch(e => res.status(400).send(e));
}

/** * Get Specialite list */
function list(req, res, next) {
  Specialite.find()
    .then(specialites => res.json(specialites))
    .catch(e => next(e));
}

/** * Delete Specialite */
function remove(req, res) {
  Specialite.findOneAndDelete(req.params.specialiteId)
    .then(specialite => res.json(specialite))
    .catch(e => res.status(400).send(e));
}

module.exports = { create, update, list, remove };
