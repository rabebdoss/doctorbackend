const express = require("express");
const specialiteCtrl = require("./specialite.controller");

const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** GET /api/specialites - Get list of specialites */
  .get(specialiteCtrl.list)

  /** POST /api/specialites - Create new specialite */
  .post(specialiteCtrl.create);

router
  .route("/:specialiteId")
  /** PUT /api/specialites/5cb0a81bee45a4139cd88fdd - Update specialite */
  .put(specialiteCtrl.update)

  /** DELETE /api/specialites/5cb0a81bee45a4139cd88fdd - Delete specialite */
  .delete(specialiteCtrl.remove);

module.exports = router;
