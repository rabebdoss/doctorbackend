const Ville = require("./ville.model");
var sha512 = require("js-sha512");

/** * Create new ville */
function create(req, res, next) {
  let ville = new Ville();
  ville.name = req.body.name;
  ville.save(e => {
    if (e) {
      res.status(406).send(e);
    } else {
      res.status(201).json(ville);
    }
  });
}

/** * Update existing ville  */
function update(req, res, next) {
  Ville.findByIdAndUpdate(
    req.params.villeId,
    {
      name: req.body.name
    },
    { new: true }
  )
    .then(ville => res.json(ville))
    .catch(e => res.status(400).send(e));
}

/** * Get ville list */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Ville.list({ limit, skip })
    .then(villes => res.json(villes))
    .catch(e => next(e));
}
/** * Delete ville */
function remove(req, res) {
  Ville.findOneAndDelete(req.params.villeId)
    .then(ville => res.json(ville))
    .catch(e => res.status(400).send(e));
}

module.exports = { create, update, list, remove };
