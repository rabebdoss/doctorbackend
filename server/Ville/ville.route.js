const express = require("express");
const villeCtrl = require("./ville.controller");
const router = express.Router();

router
  .route("/")
  /** GET /api/villes - Get list of villes */
  .get(villeCtrl.list)

  /** POST /api/villes - Create new villes */
  .post(villeCtrl.create);

router
  .route("/:villeId")
  /** PUT /api/villes/5cb0a81bee45a4139cd88fdd - Update ville */
  .put(villeCtrl.update)
  /** DELETE /api/villes/5cb0a81bee45a4139cd88fdd - Delete ville */
  .delete(villeCtrl.remove);

module.exports = router;
