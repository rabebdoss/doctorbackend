const Promise = require("bluebird");
const mongoose = require("mongoose");
const httpStatus = require("http-status");
/**
 * User Schema
 */
const VilleSchema = new mongoose.Schema({
  name: {
    type: String
    //require: true
  }
});
/** * Methods */
VilleSchema.method({});

/** * Statics */
VilleSchema.statics = {
  /**   * List Ville in descending order of 'createdAt' timestamp.     */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

module.exports = mongoose.model("Ville", VilleSchema);
