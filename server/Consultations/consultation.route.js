const express = require("express");
const validate = require("express-validation");
const consultationCtrl = require("./consultation.controller");

const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** POST /api/consultations - Create new consultation */
  .post(consultationCtrl.create);

router.route("/").get(consultationCtrl.list);

router
  .route("/:id")
  .get(consultationCtrl.getConsultation)
  .put(consultationCtrl.update);

router
  .route("/medecin/:idMedecin")
  .get(consultationCtrl.getlistPatientbyMedecin);

router
  .route("/patient/:idPatient")
  .get(consultationCtrl.getlistMedecinbyPatient);

router
  .route("/patient/:idPatient/medecin/:idMedecin")
  .get(consultationCtrl.getlistConsultationPatient);

module.exports = router;
