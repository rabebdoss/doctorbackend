const Consultation = require("./consultation.model");
const User = require("../User/user.model");

function create(req, res, next) {
  let consultation = new Consultation();
  consultation.rendezVous = req.body.rendezVous;
  consultation.patient = req.body.patient;
  consultation.medecin = req.body.medecin;
  consultation.observation = req.body.observation;
  consultation.medicaments = req.body.medicaments;
  consultation.save(e => {
    if (e) {
      res.status(406).send(e);
    } else {
      res.status(201).json(consultation);
    }
  });
}
function update(req, res, next) {
  Consultation.findByIdAndUpdate(
    req.params.id,
    {
      etat: req.body.etat,
      observation: req.body.observation,
      medicaments: req.body.medicaments
    },
    { new: true }
  )
    .then(consultation => {
      res.json(consultation);
    })
    .catch(e => res.status(400).send(e));
}

function getlistConsultationPatient(req, res, next) {
  Consultation.find({
    patient: req.params.idPatient,
    medecin: req.params.idMedecin
  })
    .populate("rendezVous", "date time")
    .then(consultation => res.json(consultation))
    .catch(e => res.status(400).send(e));
}

function getConsultation(req, res, next) {
  Consultation.findById(req.params.id)
    .populate("patient", "first_name last_name userImage mobile_number ville")
    .populate("medicaments.medicament")
    .then(consultation => {
      res.json(consultation);
    })
    .catch(e => res.status(400).send(e));
}

/**** get getlistPatientbyMedecin */
const getlistPatientbyMedecin = async (req, res, next) => {
  const ids = await Consultation.find({
    medecin: req.params.idMedecin
  }).distinct("patient");
  const tab = [];
  if (ids.length === 0) {
    return res.json(tab);
  } else {
    let index = 0;
    ids.forEach(async id => {
      const user = await User.findById(id).select(
        "first_name last_name userImage mobile_number "
      );
      tab.push(user);
      index = index + 1;
      //console.log("index", index, "ids", ids.length);

      if (index === ids.length) {
        return res.json(tab);
      }
    });
  }
};
/**** get getlistMedecinbyPatient */
async function getlistMedecinbyPatient(req, res, next) {
  const ids = await Consultation.find({
    patient: req.params.idPatient
  }).distinct("medecin");
  const tab = [];
  if (ids.length === 0) {
    return res.json(tab);
  } else {
    let index = 0;
    ids.forEach(async id => {
      const user = await User.findById(id).select(
        "first_name last_name userImage mobile_number "
      );
      tab.push(user);
      index = index + 1;
      if (index + 1 === ids.length) {
        return res.json(tab);
      }
    });
  }
}
/** * Get consultation list */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;

  Consultation.list({ limit, skip })
    .then(consultation => res.json(consultation))
    .catch(e => next(e));
}

module.exports = {
  create,
  list,
  getlistPatientbyMedecin,
  getlistConsultationPatient,
  getConsultation,
  update,
  getlistMedecinbyPatient
};
