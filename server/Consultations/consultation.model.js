const Promise = require("bluebird");
const mongoose = require("mongoose");
const httpStatus = require("http-status");

/** * Consultation Schema */
const ConsultationSchema = new mongoose.Schema({
  rendezVous: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "RendezVous"
  },
  patient: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  medecin: { type: mongoose.Schema.Types.ObjectId, ref: "User" },

  observation: { type: String, default: "" },
  etat: { type: String, default: "new" },
  medicaments: [
    {
      medicament: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Medicament"
      },
      dose: { type: Number, default: 0 },
      period: { type: Number, default: 1 },
      numberPerDay: { type: Number, default: 1 }
    }
  ]
});
/** * Methods */
ConsultationSchema.method({});

/** * Statics */
ConsultationSchema.statics = {
  /**   * List Consultations in descending order of 'createdAt' timestamp   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};

module.exports = mongoose.model("Consultation", ConsultationSchema);
