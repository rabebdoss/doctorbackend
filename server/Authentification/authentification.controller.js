const jwt = require("jsonwebtoken");
var sha512 = require("js-sha512");

let User = require("../User/user.model");

/** * Returns jwt token if valid username and password is provided  */
function login(req, res, next) {
  User.findOne({ email: req.body.email }).then(user => {
    if (user && user.password === sha512(req.body.password)) {
      token = jwt.sign(
        { user },
        "SuperSecRetKey",
        { algorithm: "HS512" },
        (err, token) => {
          res.json({ token, _id: user._id });
        }
      );
    } else if (user && user.password !== sha512(req.body.password)) {
      return res
        .status(404)
        .json({ type: "password", message: "Password incorrect" });
    } else {
      return res.status(404).json({ type: "email", message: "User not Found" });
    }
  });
}
/** * This is a protected route. Will return random number only if jwt token is provided in header */
function getRandomNumber(req, res) {
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

function loginAdmin(req, res, next) {
  User.findOne({ email: req.body.email }).then(user => {
    if (
      user &&
      user.user_type === "Admin" &&
      user.password === sha512(req.body.password)
    ) {
      token = jwt.sign(
        { user },
        "SuperSecRetKey",
        { algorithm: "HS512" },
        (err, token) => {
          res.json({ token, _id: user._id });
        }
      );
    } else if (user && user.password !== sha512(req.body.password)) {
      return res
        .status(404)
        .json({ type: "password", message: "Password incorrect" });
    } else {
      return res
        .status(404)
        .json({ type: "email", message: "Admin not Found" });
    }
  });
}

module.exports = { login, getRandomNumber, loginAdmin };
