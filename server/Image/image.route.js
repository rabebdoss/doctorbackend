const express = require("express");
const imageCtrl = require("./image.controller");

const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** POST /api/images - Create new images */
  .post(imageCtrl.uploads);
module.exports = router;
