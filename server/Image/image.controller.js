const Image = require("./image.model");

const multer = require("multer");

/************** upload image *********** */

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "attachments/");
  },
  filename: function(req, file, cb) {
    const mimetypes = file.mimetype.split("/");
    cb(null, file.fieldname + "-" + Date.now() + "." + mimetypes[1]);
  }
});
var upload = multer({ storage }).array("images", 10);

/** * uploads des Images */
function uploads(req, res, next) {
  upload(req, res, err => {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      res.status(401).send(err);
    } else {
      const attachements = [];
      req.files.forEach(element => {
        attachements.push(`attachments/${element.filename}`);
      });
      let image = new Image();
      image.attachements = attachements;

      image.save(e => {
        if (e) {
          res.status(406).send(e);
        } else {
          res.status(201).json(image);
        }
      });
    }
  });
}

module.exports = { uploads };
