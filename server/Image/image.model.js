const mongoose = require("mongoose");
/**
 * Image Schema
 */
const ImageSchema = new mongoose.Schema({
  attachements: [String]
});

module.exports = mongoose.model("Images", ImageSchema);
