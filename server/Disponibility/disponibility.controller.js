const Disponibility = require("./disponibility.model");

function getGetById(req, res, next) {
  Disponibility.findOne({ userId: req.params.idUser })
    .then(disponibility => res.json(disponibility))
    .catch(e => res.status(400).send(e));
}
/** * Update existing disponibility  */
function update(req, res, next) {
  Disponibility.findOneAndUpdate(
    { userId: req.params.idUser },
    {
      period: req.body.period,
      disponibilite: req.body.disponibilite
    },
    { new: true }
  )
    .then(disponibility => res.json(disponibility))
    .catch(e => res.status(400).send(e));
}
/** * Create new Disponibility */
function create(req, res, next) {
  let disponibility = new Disponibility();
  disponibility.userId = req.body.userId;
  disponibility.period = req.body.period;
  disponibility.disponibilite = req.body.disponibilite;
  var options = {
    // Return the document after updates are applied
    new: true,
    // Create a document if one isn't found. Required
    // for `setDefaultsOnInsert`
    upsert: true,
    setDefaultsOnInsert: true
  };
  disponibility.save(e => {
    if (e) {
      res.status(406).send(e);
    } else {
      res.status(201).json(disponibility);
    }
  });
}

module.exports = { update, create, getGetById };
