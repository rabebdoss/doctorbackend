const mongoose = require("mongoose");

/**
 * disponibility Schema
 */
const DisponibilitySchema = new mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  period: {
    type: Number,
    require: true
  },
  disponibilite: [
    {
      name: { type: String, default: "Lundi" },
      startTime: { type: String, default: "" },
      endTime: { type: String, default: "" },
      active: { type: Boolean, default: true }
    },
    {
      name: { type: String, default: "Mardi" },
      startTime: { type: String, default: "" },
      endTime: { type: String, default: "" },
      active: { type: Boolean, default: true }
    },
    {
      name: { type: String, default: "Mercredi" },
      startTime: { type: String, default: "" },
      endTime: { type: String, default: "" },
      active: { type: Boolean, default: true }
    },
    {
      name: { type: String, default: "Jeudi" },
      startTime: { type: String, default: "" },
      endTime: { type: String, default: "" },
      active: { type: Boolean, default: true }
    },
    {
      name: { type: String, default: "Vendredi" },
      startTime: { type: String, default: "" },
      endTime: { type: String, default: "" },
      active: { type: Boolean, default: true }
    },
    {
      name: { type: String, default: "Samedi" },
      startTime: { type: String, default: "" },
      endTime: { type: String, default: "" },
      active: { type: Boolean, default: true }
    },
    {
      name: { type: String, default: "Dimanche" },
      startTime: { type: String, default: "" },
      endTime: { type: String, default: "" },
      active: { type: Boolean, default: true }
    }
  ]
});

module.exports = mongoose.model("Disponibility", DisponibilitySchema);
