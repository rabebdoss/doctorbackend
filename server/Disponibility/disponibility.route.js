const express = require("express");
const disponibilityCtrl = require("./disponibility.controller");
const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** PUT /api/disponibility/5cb0a81bee45a4139cd88fdd - Update disponibility */

  .post(disponibilityCtrl.create);

router
  .route("/:idUser")
  .get(disponibilityCtrl.getGetById)
  .put(disponibilityCtrl.update);

module.exports = router;
